////////////////////////////////////////////////////////////////////////////////
// Detect the tools needed to build an Android project
////////////////////////////////////////////////////////////////////////////////

package weld

import (
	"log"
	"os"
	"os/exec"
	"strings"
	"syscall"
)

////////////////////////////////////////////////////////////////////////////////
// Public API
////////////////////////////////////////////////////////////////////////////////

type toolParams struct {
	envvar bool
	t      toolType
	cmd    string
	args   []string
}

// Tries to find the paths to the various command line tools used to build on
// Android. If a specific tool cannot be detected emits a warning message but it
// doesn't fail.
func Detect(tools []toolParams, verbose bool) map[toolType]string {
	result := make(map[toolType]string)

	for _, tool := range tools {
		if tool.envvar {
			if value, ok := os.LookupEnv(tool.cmd); ok {
				result[tool.t] = value
				if verbose {
					log.Printf("--- Checking value of $%s... ok\n", tool.cmd)
				}
			} else {
				log.Printf("--- Checking value of $%s... failed (warning, this environment variable is not set)\n", tool.cmd)
			}
		} else {
			if value, ok := detectTool(tool.cmd, tool.args...); ok {
				result[tool.t] = value
				if verbose {
					log.Printf("--- Checking for runnable %s... ok\n", tool.cmd)
				}
			} else {
				log.Printf("--- Checking for runnable %s... failed (error: %s)\n", tool.cmd, value)
			}
		}
	}
	return result
}

////////////////////////////////////////////////////////////////////////////////
// Implementation details
////////////////////////////////////////////////////////////////////////////////

// Try to execute the specified binary and check the output. A succesful run
// means that the exit code is less or equal than 2. This is because some tools
// don't provide a "--version" argument or equivalent to check availability.
// We don't care to get the arguments wrong, we just want to make sure that the
// binary actually runs.
func detectTool(bin string, args ...string) (string, bool) {
	cmd := exec.Command(bin, args...)
	if err := cmd.Run(); err == nil {
		return getToolFullPath(bin)
	} else {
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			exitCode := waitStatus.ExitStatus()
			if exitCode <= 2 {
				return getToolFullPath(bin)
			}
		}
		return err.Error(), false
	}
}

// Get the full path of an executable command in the system.
// Returns the path and true if found, otherwise an empty string and false.
func getToolFullPath(bin string) (string, bool) {
	cmd := exec.Command("which", bin)
	output, err := cmd.Output()
	if err != nil {
		return "", false
	}
	return strings.TrimSpace(string(output)), true
}
