package main

import (
	w "bitbucket.org/jayel/weld/pkg"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
)

func showHelp(command string) {
	switch command {
	case "init":
		fmt.Printf(`Initializes an Android project built with Weld.
Usage: weld [-v] [ -p=<package> ] [ -n=<name> ] [ -t=<platform> ] [ -r=<root> ] [ -k=<keystore> ] [ -kp=<keystore-password> ] [ -ka=<keystore-alias> ] init

Options:
    -k    Keystore file
    -kp   Password of keystore file
    -ka   Alias in keystore to use
    -n    Name of the application [default = "MyApp"]
    -p    Java package for this project [default = "com.example.myproject"]
    -r    Root of the project [default = current directory]
    -t    Target platform / API level [default = lowest version currently installed]
    -v    Verbose output
`)
	case "build":
		fmt.Printf(`Compile an Android project.
Usage: weld [-v] build 

Options:
    -v  Verbose output
`)
	case "package":
		fmt.Printf(`Package an Android project.
Usage: weld package [-v]

Options:
    -v  Verbose output
`)
	case "launch":
		fmt.Printf(`Install and launch the project on an Android device.
Usage: weld [-v] [ -s <device> | -d | -e ] launch 

Options:
	-v	Verbose output

When more than one device available, use one of the following mutually exclusive options:
    -d  Use the only USB-attached device
    -e  Use the only running emulator instance (default if none selected)
    -s  Serial number of the Android device to use
`)
	case "clean":
		fmt.Printf(`Remove all compiled and generated files.
Usage: weld clean 
`)
	default:
		fmt.Printf(`Usage: weld <command> [<args>]

Available commands:
    build       Compile the project
    clean       Remove all compiled binaries and generated files
    help        Show help for a specific command, or this message
    init        Initialize an Android project in the specified folder
    launch      Launch the project on a device
    package     Create an APK package
`)
	}
}

func getWeldFile() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return path.Join(dir, "weld.it")
}

func main() {
	log.SetFlags(0)
	log.SetOutput(os.Stdout)
	pkg := flag.String("p", "com.example.myproject", "Java package")
	name := flag.String("n", "MyApp", "Name of the application")
	root := flag.String("r", ".", "Root folder")
	verbose := flag.Bool("v", false, "Verbose output")
	keystore := flag.String("k", "", "Keystore file")
	keystorePassword := flag.String("kp", "", "Keystore password")
	keystoreAlias := flag.String("ka", "", "Keystore alias")
	usb := flag.Bool("d", false, "Use the only USB-attached device")
	emulator := flag.Bool("e", false, "Use the only running emulator instance")
	serial := flag.String("s", "", "Android device serial number")
	flag.Parse()

	if flag.NArg() < 1 {
		showHelp("")
		return
	}
	args := flag.Args()
	switch args[0] {
	case "init":
		project := w.NewProject()
		project.Init(*root, *pkg, *name, *keystore, *keystorePassword, *keystoreAlias, *verbose)
	case "build":
		project := w.LoadProject(getWeldFile())
		project.Build(*verbose)
	case "package":
		project := w.LoadProject(getWeldFile())
		project.Package(*verbose)
	case "launch":
		project := w.LoadProject(getWeldFile())
		count := 0
		if *usb {
			count++
		}
		if *emulator {
			count++
		}
		if len(*serial) > 0 {
			count++
		}
		switch count {
		case 0:
			project.Launch(*usb, true, *serial, *verbose)
		case 1:
			project.Launch(*usb, *emulator, *serial, *verbose)
		default:
			showHelp(args[0])
		}
	case "clean":
		project := w.LoadProject(getWeldFile())
		project.Clean()
	case "help":
		if len(args) >= 2 {
			showHelp(args[1])
		} else {
			showHelp("")
		}
	default:
		showHelp("")
	}
}
