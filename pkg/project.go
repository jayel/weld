////////////////////////////////////////////////////////////////////////////////
// The types here represent the base types of a generic project to build. These
// are further specialized and implemented in other files. Currently only
// Android is supported.
////////////////////////////////////////////////////////////////////////////////

package weld

type toolType int16

const (
	toolAapt toolType = iota
	toolAdb
	toolDx
	toolJarsigner
	toolJavac
	toolZipalign
	toolEnvAndroidSdk
)

type Project interface {
	// Initialize the project. Creates any necessary files and folders to have a
	// minimum project.
	Init(root string, pkg string, appName string, keystore string,
		keystorePassword string, keystoreAlias string, verbose bool)

	// Build the project.
	Build(verbose bool)

	// Package the project. Depending on the platform this may have no effect
	// other than what is specified in the Weld Script files.
	Package(verbose bool)

	// Launch the project in an emulator, if applicable.
	Launch(usb bool, emulator bool, serial string, verbose bool)

	// Launch the project in an emulator, if applicable.
	Clean()

	// Return the full path for the specified tool.
	Tool(t toolType) string
}

func toolToStr(t toolType) string {
	switch t {
	case toolAapt:
		return "aapt"
	case toolAdb:
		return "adb"
	case toolDx:
		return "dx"
	case toolJarsigner:
		return "jarsigner"
	case toolZipalign:
		return "zipalign"
	case toolJavac:
		return "javac"
	case toolEnvAndroidSdk:
		return "ANDROID_SDK_ROOT"
	}
	panic("Unknown tool value")
}

func strToTool(str string) (toolType, bool) {
	switch str {
	case "aapt":
		return toolAapt, true
	case "adb":
		return toolAdb, true
	case "dx":
		return toolDx, true
	case "jarsigner":
		return toolJarsigner, true
	case "zipalign":
		return toolZipalign, true
	case "javac":
		return toolJavac, true
	case "ANDROID_SDK_ROOT":
		return toolEnvAndroidSdk, true
	}
	return toolAapt, false
}
