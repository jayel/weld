////////////////////////////////////////////////////////////////////////////////
// Common utility functions used in different files.
// All functions are internal to the package, there are no exported symbols.
////////////////////////////////////////////////////////////////////////////////

package weld

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"syscall"
)

// Execute the specified command as a separate process and print the output.
// If the process fails for some reason, the program stops execution.
func runOrFail(exit bool, printOut bool, name string, arg ...string) error {
	cmd := exec.Command(name, arg...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		if exit {
			log.SetOutput(os.Stderr)
			log.Print(string(output))
			log.Fatal(err)
		} else {
			return err
		}
	}
	if printOut {
		log.Printf("%s\n", output)
	}
	return nil
}

// Checks that the specified tool is set, and if not prints an error message and
// the program stops execution.
func assertToolExists(tool string, msg string) {
	if len(tool) == 0 {
		log.Fatal(msg)
	}
}

// Remove the path (either file or directory) if it exists.
func removeIfExists(path string) {
	if file, err := os.Stat(path); !os.IsNotExist(err) {
		switch mode := file.Mode(); {
		case mode.IsDir():
			if err = os.RemoveAll(path); err != nil {
				log.SetOutput(os.Stderr)
				log.Fatal(err)
			}
		case mode.IsRegular():
			if err = os.Remove(path); err != nil {
				log.SetOutput(os.Stderr)
				log.Fatal(err)
			}
		}
	}
}

func findFiles(ext string) []string {
	pathS, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	var files []string
	filepath.Walk(pathS, func(path string, f os.FileInfo, _ error) error {
		if !f.IsDir() {
			r, err := regexp.MatchString(ext, f.Name())
			if err == nil && r {
				files = append(files, path)
			}
		}
		return nil
	})
	return files
}

func getExitCode(err error) int {
	if err == nil {
		return 0
	}
	if exitError, ok := err.(*exec.ExitError); ok {
		waitStatus := exitError.Sys().(syscall.WaitStatus)
		return waitStatus.ExitStatus()
	}
	panic("not an exit error")
}

// Creates a directory if it doesn't already exist.
func createDir(path string, verbose bool) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		if verbose {
			log.Printf("--- Creating %s\n", path)
		}
		os.MkdirAll(path, os.ModePerm)
	}
}

// Creates the file only if it doesn't already exist.
func createBinFile(verbose bool, filename string, data []byte) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		if verbose {
			log.Printf("--- Creating file %s\n", filename)
		}
		if err := ioutil.WriteFile(filename, data, 0644); err != nil {
			log.SetOutput(os.Stderr)
			log.Fatal(err)
		}
	}
}

// Creates the file only if it doesn't already exist.
func createFile(verbose bool, filename string, data string, args ...interface{}) {
	text := fmt.Sprintf(data, args...)
	createBinFile(verbose, filename, []byte(text))
}

// Load an embedded text file asset or exit the program.
func loadTextAsset(name string) string {
	data, err := Asset(name)
	if err != nil {
		log.SetOutput(os.Stderr)
		log.Fatal(err)
	}
	return string(data)
}

// Load an embedded binary file asset or exit the program.
func loadBinAsset(name string) []byte {
	data, err := Asset(name)
	if err != nil {
		log.SetOutput(os.Stderr)
		log.Fatal(err)
	}
	return data
}
