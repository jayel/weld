package weld

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"sort"
	"testing"
)

func TestSortByStage(t *testing.T) {
	actions := [...]BuildAction{
		{stepBuild, stageAfter, ""},
		{stepBuild, stageBefore, ""},
	}
	sort.Sort(ByPhase(actions[:]))
	assert.Equal(t, stageBefore, actions[0].stage)
	assert.Equal(t, stageAfter, actions[1].stage)
}

func TestSortByStep(t *testing.T) {
	actions := [...]BuildAction{
		{stepPackage, stageAfter, ""},
		{stepLaunch, stageBefore, ""},
		{stepBuild, stageBefore, ""},
	}
	sort.Sort(ByPhase(actions[:]))
	assert.Equal(t, stepBuild, actions[0].step)
	assert.Equal(t, stepPackage, actions[1].step)
	assert.Equal(t, stepLaunch, actions[2].step)
}

func TestGetActions(t *testing.T) {
	actions := [...]BuildAction{
		{stepBuild, stageBefore, ""},
		{stepPackage, stageBefore, ""},
		{stepPackage, stageAfter, ""},
		{stepLaunch, stageBefore, ""},
		{stepLaunch, stageBefore, ""},
		{stepLaunch, stageAfter, ""},
	}
	tests := [...]struct {
		step  actionStep
		stage actionStage
		count int
	}{
		{stepBuild, stageBefore, 1},
		{stepBuild, stageAfter, 0},
		{stepPackage, stageBefore, 1},
		{stepPackage, stageAfter, 1},
		{stepLaunch, stageBefore, 2},
		{stepLaunch, stageAfter, 1},
	}
	for _, test := range tests {
		result := getActionsForPhase(test.step, test.stage, actions[:])
		assert.Equal(t, test.count, len(result), fmt.Sprintf("step: %v, stage: %v", test.step, test.stage))
		for _, action := range result {
			assert.Equal(t, test.step, action.step)
			assert.Equal(t, test.stage, action.stage)
		}
	}
}

func TestGetActionsEmpty(t *testing.T) {
	var actions []BuildAction
	result := getActionsForPhase(stepBuild, stageBefore, actions)
	assert.Equal(t, 0, len(result))
}
