////////////////////////////////////////////////////////////////////////////////
// Initialization of an Android project. Creates the bare minimum files and
// directories to start a project.
////////////////////////////////////////////////////////////////////////////////

package weld

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
)

// Contains settings like the target ID, and the paths to the necessary tools to
// build and launch a project.
type android struct {
	// Root directory of the project. All files and folders go under this one.
	projectRoot string
	// Generated source directory, derived from the package.
	sourceRoot string
	// Name of the application.
	appName string
	// Full path to the keystore file used to sign APKs.
	keystore string
	// Password of the keystore file.
	keystorePassword string
	// Alias of the keystore to use.
	keystoreAlias string
	// Java package string.
	pkg string
	// Target Android platform / API level.
	targetPlatform string
	// Paths to various tools (aapt, javac, etc).
	tools map[toolType]string
	// Actions to perform at different stages of the build.
	actions []BuildAction
}

////////////////////////////////////////////////////////////////////////////////
// Public API
////////////////////////////////////////////////////////////////////////////////

func NewProject() Project {
	env := new(android)
	env.tools = make(map[toolType]string)
	return env
}

func LoadProject(path string) Project {
	script, err := ParseFile(path)
	if err != nil {
		log.SetOutput(os.Stderr)
		log.Fatal(err)
	}
	project := NewProject()
	android, ok := project.(*android)
	if !ok {
		panic(ok)
	}
	android.actions = script.Actions
	android.appName = script.StrVars["appName"]
	android.keystore = script.StrVars["keystore"]
	android.keystorePassword = script.StrVars["keystorePassword"]
	android.keystoreAlias = script.StrVars["keystoreAlias"]
	android.pkg = script.StrVars["pkg"]
	android.targetPlatform = script.StrVars["targetPlatform"]
	for k, v := range script.StrVars {
		if tool, ok := strToTool(k); ok {
			android.tools[tool] = v
		}
	}
	return android
}

func (env *android) Tool(t toolType) string {
	value, ok := env.tools[t]
	if !ok {
		log.SetOutput(os.Stderr)
		log.Fatalf("Error: cannot find %s, build stopped\n", toolToStr(t))
	}
	return value
}

func (env *android) Init(root string, pkg string, appName string,
	keystore string, keystorePassword string, keystoreAlias string, verbose bool) {
	env.projectRoot = root
	env.sourceRoot = path.Join(root, "src", strings.Replace(pkg, ".", "/", -1))
	env.pkg = pkg
	env.appName = appName
	env.keystore = keystore
	env.keystorePassword = keystorePassword
	env.keystoreAlias = keystoreAlias
	env.detectTools(verbose)
	env.createFolders(verbose)
	env.createFiles(verbose)
}

func (env *android) Build(verbose bool) {
	env.regenerateObjectFolders()

	env.runActionsForPhase(stepBuild, stageBefore)
	classpath := path.Join(env.Tool(toolEnvAndroidSdk), "platforms", env.targetPlatform, "android.jar")
	if verbose {
		log.Printf("Generating resources... ")
	}
	// Generate R.java
	runOrFail(true, false, env.Tool(toolAapt), "package", "-f", "-m", "-J", "gen",
		"-M", "AndroidManifest.xml", "-S", "res", "-I", classpath)
	if verbose {
		log.Println("done")
		log.Printf("Compiling sources... ")
	}
	// Compile Java sources
	sourceFiles := findFiles("java")
	javacArgs := []string{"-classpath", classpath, "-sourcepath", "'src:gen'", "-d", "bin",
		"-target", "1.8", "-source", "1.8"}
	javacArgs = append(javacArgs, sourceFiles...)
	runOrFail(true, false, env.Tool(toolJavac), javacArgs...)
	if verbose {
		log.Println("done")
		log.Printf("Dexing classes... ")
	}
	// Convert JavaVM to Dalvik VM
	runOrFail(true, false, env.Tool(toolDx), "--dex", "--output=classes.dex", "bin")
	if verbose {
		log.Println("done")
	}
	env.runActionsForPhase(stepBuild, stageAfter)
}

func (env *android) Package(verbose bool) {
	env.runActionsForPhase(stepPackage, stageBefore)
	classpath := path.Join(env.Tool(toolEnvAndroidSdk), "platforms",
		env.targetPlatform, "android.jar")
	if verbose {
		log.Print("Generating APK file... ")
	}
	// Generate APK and add .dex file to it
	runOrFail(true, false, env.Tool(toolAapt), "package", "-f", "-M", "AndroidManifest.xml",
		"-S", "res", "-I", classpath, "-F", env.appName+".apk.unaligned")
	runOrFail(true, false, env.Tool(toolAapt), "add", env.appName+".apk.unaligned", "classes.dex")
	if verbose {
		log.Println("done")
		log.Print("Signing APK... ")
	}
	// Sign the APK package
	runOrFail(true, false, env.Tool(toolJarsigner), "-sigalg", "SHA1withRSA", "-digestalg",
		"SHA1", "-keystore", env.keystore, "-storepass", env.keystorePassword,
		env.appName+".apk.unaligned", env.keystoreAlias)
	if verbose {
		log.Println("done")
		log.Print("Aligining APK... ")
	}
	// Align it to 32 bit
	runOrFail(true, false, env.Tool(toolZipalign), "-f", "4", env.appName+".apk.unaligned",
		env.appName+"-debug.apk")
	if verbose {
		log.Println("done")
	}
	env.runActionsForPhase(stepPackage, stageAfter)
}

func (env *android) Launch(usb bool, emulator bool, serial string, verbose bool) {
	env.runActionsForPhase(stepLaunch, stageBefore)
	adbDevice := getLaunchDeviceArgs(usb, emulator, serial)
	if verbose {
		log.Print("Uninstalling old package... ")
	}

	// Stop launching if there's no device available
	env.deviceAvailable(true)

	// Uninstall previous package just in case
	args := adbDevice
	args = append(args, "uninstall", env.pkg)
	runOrFail(true, false, env.Tool(toolAdb), args...)
	if verbose {
		log.Println(" done")
		log.Print("Installing new package on device... ")
	}
	// Install it
	args = adbDevice
	args = append(args, "install", "-f", env.appName+"-debug.apk")
	runOrFail(true, false, env.Tool(toolAdb), args...)
	if verbose {
		log.Println("done")
		log.Print("Launching application... ")
	}
	// Launch it on the device
	args = adbDevice
	args = append(args, "shell", "am", "start", "-n", env.pkg+"/."+env.appName)
	runOrFail(true, false, env.Tool(toolAdb), args...)
	if verbose {
		log.Println("done")
	}
	env.runActionsForPhase(stepLaunch, stageAfter)
}

func (env *android) Clean() {
	removeIfExists(path.Join(env.projectRoot, "gen"))
	removeIfExists(path.Join(env.projectRoot, "bin"))
	removeIfExists(env.appName + ".apk.unaligned")
	removeIfExists(env.appName + "-debug.apk")
	removeIfExists("classes.dex")
}

////////////////////////////////////////////////////////////////////////////////
// Implementation details
////////////////////////////////////////////////////////////////////////////////

var androidFolders = [...]string{
	"bin",
	"docs",
	"lib",
	"obj",
	"res/drawable-hdpi",
	"res/drawable-ldpi",
	"res/drawable-mdpi",
	"res/drawable-xhdpi",
	"res/layout",
	"res/values",
}

func (env *android) detectTools(verbose bool) {
	tools := []toolParams{
		{false, toolAapt, "aapt", []string{"v"}},
		{false, toolAdb, "adb", []string{"version"}},
		{false, toolDx, "dx", []string{"--version"}},
		{false, toolJarsigner, "jarsigner", []string{"--help"}},
		{false, toolJavac, "javac", []string{"-version"}},
		{false, toolZipalign, "zipalign", nil},
		{true, toolEnvAndroidSdk, "ANDROID_SDK_ROOT", nil},
	}
	env.tools = Detect(tools, verbose)
	sdkRoot, ok := env.tools[toolEnvAndroidSdk]
	if ok {
		env.targetPlatform = detectPlatform(sdkRoot, verbose)
	} else {
		log.Print("--- Warning: cannot detect default platform without $ANDROID_SDK_ROOT")
	}
}

// Creates the initial default files in an Android project: the manifest,
// minimal resource files, and main activity skeleton.
func (env *android) createFiles(verbose bool) {
	createFile(verbose, path.Join(env.projectRoot, "AndroidManifest.xml"),
		loadTextAsset("assets/AndroidManifest.xml"), env.pkg, env.appName)
	createFile(verbose, path.Join(env.projectRoot, "res/values/strings.xml"),
		loadTextAsset("assets/res/values/strings.xml"), env.appName)
	createFile(verbose, path.Join(env.projectRoot, "res/layout/main.xml"),
		loadTextAsset("assets/res/layout/main.xml"))
	activity_path := path.Join(env.sourceRoot, env.appName) + ".java"
	createFile(verbose, activity_path, loadTextAsset("assets/Main.java"),
		env.pkg, env.appName)
	createFile(verbose, path.Join(env.projectRoot, "weld.it"), loadTextAsset("assets/weld.it"),
		env.dumpVariables())
	createBinFile(verbose, path.Join(env.projectRoot, "res/drawable-xhdpi/ic_launcher.png"),
		loadBinAsset("assets/res/drawable-xhdpi/ic_launcher.png"))
	createBinFile(verbose, path.Join(env.projectRoot, "res/drawable-hdpi/ic_launcher.png"),
		loadBinAsset("assets/res/drawable-hdpi/ic_launcher.png"))
	createBinFile(verbose, path.Join(env.projectRoot, "res/drawable-mdpi/ic_launcher.png"),
		loadBinAsset("assets/res/drawable-mdpi/ic_launcher.png"))
	createBinFile(verbose, path.Join(env.projectRoot, "res/drawable-ldpi/ic_launcher.png"),
		loadBinAsset("assets/res/drawable-ldpi/ic_launcher.png"))
}

// Creates the initial default folders in an Android project.
func (env *android) createFolders(verbose bool) {
	for _, p := range androidFolders {
		createDir(path.Join(env.projectRoot, p), verbose)
	}
	pkg_path := strings.Replace(env.pkg, ".", "/", -1)
	createDir(path.Join(env.projectRoot, "src", pkg_path), verbose)
}

// Get the lowest platform available in the system.
// Uses $ANDROID_SDK_ROOT to see what plaforms are installed. If not set emits a
// warning, that will at some point in the build convert to an error.
func detectPlatform(androidSdkRoot string, verbose bool) (platform string) {
	ls := exec.Command("ls", path.Join(androidSdkRoot, "platforms"))
	sort := exec.Command("sort", "-nr")
	tail := exec.Command("tail", "-1")

	r1, w1 := io.Pipe()
	ls.Stdout = w1
	sort.Stdin = r1

	r2, w2 := io.Pipe()
	sort.Stdout = w2
	tail.Stdin = r2

	var b2 bytes.Buffer
	tail.Stdout = &b2

	ls.Start()
	sort.Start()
	tail.Start()
	ls.Wait()
	w1.Close()
	sort.Wait()
	w2.Close()
	err := tail.Wait()
	if err != nil {
		log.Printf("--- Warning: cannot detect target platform, check $ANDROID_SDK_ROOT\n")
	} else {
		platform = strings.TrimSpace(string(b2.Bytes()))
		if verbose {
			log.Printf("--- Target platform: %s\n", platform)
		}
	}
	r1.Close()
	r2.Close()
	return strings.TrimSpace(platform)
}

// Removes current objects (generated files, apks, etc) and folders (bin, gen)
// and creates the folders again.
// If it fails at any point the program stops execution, since there's some
// problem with the host.
func (env *android) regenerateObjectFolders() {
	env.Clean()
	// Create folders
	if err := os.MkdirAll(path.Join(env.projectRoot, "gen"), 0755); err != nil {
		log.SetOutput(os.Stderr)
		log.Fatal(err)
	}
	if err := os.MkdirAll(path.Join(env.projectRoot, "bin"), 0755); err != nil {
		log.SetOutput(os.Stderr)
		log.Fatal(err)
	}
}

func (env *android) runActionsForPhase(step actionStep, stage actionStage) {
	actions := getActionsForPhase(step, stage, env.actions)
	for _, action := range actions {
		args := strings.Fields(action.command)
		cmd := exec.Command(args[0], args[1:]...)
		cmd.Start()
		cmd.Wait()
	}
}

// Get the appropriate device selection flags for ADB.
// Only one should be set at the same time. If more than one value is set, it
// returns flags for the first argument set to true.
func getLaunchDeviceArgs(usb bool, emulator bool, serial string) []string {
	if usb {
		return []string{"-d"}
	}
	if emulator {
		return []string{"-e"}
	}
	if len(serial) > 0 {
		return []string{"-s", serial}
	}
	return []string{"-e"}
}

func (env *android) dumpVariables() string {
	str := fmt.Sprintf(`appName = "%s"
pkg = "%s"
keystore = "%s"
keystorePassword = "%s"
keystoreAlias = "%s"
targetPlatform = "%s"
`, env.appName, env.pkg, env.keystore, env.keystorePassword, env.keystoreAlias, env.targetPlatform)
	for k, v := range env.tools {
		str += fmt.Sprintf("%s = \"%s\"\n", toolToStr(k), v)
	}
	return str
}

// Check there's a device available via ADB
func (env *android) deviceAvailable(required bool) bool {
	cmd := exec.Command(env.Tool(toolAdb), "shell", "getprop", "sys.boot_completed")
	out, err := cmd.Output()
	if getExitCode(err) != 0 || strings.TrimSpace(string(out)) != "1" {
		if required {
			log.SetOutput(os.Stderr)
			log.Fatal("--- Error: No devices available via ADB, can't launch the project")
		} else {
			return false
		}
	}
	return true
}
