package weld

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

func createTestFile(content string, filename string) {
	ioutil.WriteFile(filename, []byte(content), 0644)
}

func TestLoadProject(t *testing.T) {
	content := `
appName = "test"
pkg = "com.test.project"
javac = "/usr/javac"
`
	assert := assert.New(t)
	ioutil.WriteFile("dummy.test", []byte(content), 0644)
	p := LoadProject("dummy.test")
	a, ok := p.(*android)
	assert.Equal(true, ok)
	assert.Equal("test", a.appName)
	assert.Equal("com.test.project", a.pkg)
	assert.Equal("", a.keystore)
	assert.Equal("", a.keystorePassword)
	assert.Equal("", a.keystoreAlias)
	assert.Equal("", a.targetPlatform)
	assert.Equal("", a.tools[toolAapt])
	assert.Equal("", a.tools[toolAdb])
	assert.Equal("", a.tools[toolDx])
	assert.Equal("", a.tools[toolJarsigner])
	assert.Equal("/usr/javac", a.tools[toolJavac])
	assert.Equal("", a.tools[toolZipalign])
	assert.Equal("", a.tools[toolEnvAndroidSdk])
	os.Remove("dummy.test")
}

func TestNewProject(t *testing.T) {
	assert := assert.New(t)
	p := NewProject()
	a, ok := p.(*android)
	assert.Equal(true, ok)
	assert.Equal("", a.appName)
	assert.Equal("", a.keystore)
	assert.Equal("", a.keystorePassword)
	assert.Equal("", a.keystoreAlias)
	assert.Equal("", a.targetPlatform)
	assert.Equal("", a.tools[toolAapt])
	assert.Equal("", a.tools[toolAdb])
	assert.Equal("", a.tools[toolDx])
	assert.Equal("", a.tools[toolJarsigner])
	assert.Equal("", a.tools[toolJavac])
	assert.Equal("", a.tools[toolZipalign])
	assert.Equal("", a.tools[toolEnvAndroidSdk])
}
