////////////////////////////////////////////////////////////////////////////////
// Handles parsing of the weld config file
////////////////////////////////////////////////////////////////////////////////

// TODO: add configuration items for keystore to script grammar

package weld

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"unicode"
)

////////////////////////////////////////////////////////////////////////////////
// Public API
////////////////////////////////////////////////////////////////////////////////

type BuildScript struct {
	Actions []BuildAction
	IntVars map[string]int
	StrVars map[string]string
}

func NewBuild() *BuildScript {
	return &BuildScript{nil, make(map[string]int), make(map[string]string)}
}

// Reads a Weld configuration file and returns a slice of actions sorted by step
// and stage:
// build [before, after] > package [before, after] > launch [before, after]
func ParseFile(path string) (build *BuildScript, err error) {
	script, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}
	build, err = ParseString(string(script))
	return
}

// Parses a Weld configuration script.
// If it fails returns an empty slice and err is set to some error.
func ParseString(script string) (build *BuildScript, err error) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			err, ok = r.(error)
			if !ok {
				err = fmt.Errorf("pkg: %v", r)
			}
		}
	}()

	build, err = parseTokens(tokenize(script))
	if err != nil {
		return
	}
	sort.Sort(ByPhase(build.Actions))
	return
}

////////////////////////////////////////////////////////////////////////////////
// Implementation details
////////////////////////////////////////////////////////////////////////////////

type tokenType int

const (
	parensOpen tokenType = iota
	parensClose
	period
	assignment
	step
	stage
	command
	comment
	identifier
	strng
	number
	eof
	unknown
)

// Weld script Tokens
// -------------------------------------------------------------------------------
// step        = 'build' | 'package' | 'launch'
// stage       = 'before' | 'after'
// parensOpen  = '('
// parensClose = ')'
// period      = '.'
// variable    = [a-zA-Z]+
// assignment  = '='
// string      = "[^"]"
// number      = [0-9]+
// command     = [^)]+
// eof         = <EOF>
//
//
// Weld script Grammar
// -------------------------------------------------------------------------------
// SCRIPT	  = STATEMENT* eof
// STATEMENT  = ACTION | ASSIGNMENT
// ASSIGNMENT = variable eq VALUE
// VALUE      = string | number
// ACTION     = PHASE CMD
// PHASE      = step period stage
// CMD		  = parensOpen command parensClose

// A token identified by the tokenizer.
type token struct {
	// The token's type
	cat tokenType
	// Value of the token if applicable
	value string
}

// Match the string against possible tokens.
func matchToken(str string) *token {
	switch str {
	case "(":
		return &token{cat: parensOpen}
	case ")":
		return &token{cat: parensClose}
	case ".":
		return &token{cat: period}
	case "=":
		return &token{cat: assignment}
	case "build", "package", "launch":
		return &token{step, str}
	case "before", "after":
		return &token{stage, str}
	default:
		return &token{identifier, str}
	}
}

type predicate func(r rune) bool

// Advances the reader until the first rune that doesn't match the predicate.
func skipUntil(r *strings.Reader, pred predicate) {
	for ch, _, err := r.ReadRune(); ; {
		if err != nil {
			if err == io.EOF {
				r.UnreadRune()
				break
			}
			panic(err)
		}
		if pred(ch) {
			ch, _, err = r.ReadRune()
		} else {
			r.UnreadRune()
			break
		}
	}
}

// Consume all runes until the predicate is no longer satisfied or if it reaches
// EOF.  Returns a string containing all the runes found, and the reader
// advances until the last rune that satisfied the predicate.
func consumeWhile(r *strings.Reader, pred predicate) string {
	var str string
	var ch, _, err = r.ReadRune()

	for pred(ch) && err == nil {
		str += string(ch)
		ch, _, err = r.ReadRune()
	}
	if err != nil && err != io.EOF {
		panic(err)
	}
	r.UnreadRune()
	return str
}

// Advance the reader until the first non-whitespace, non-comment rune.
func skipComments(r *strings.Reader) {
	// If starting a comment, ignore everything until new line and all
	// consecutive comments.
	skipUntil(r, unicode.IsSpace)
	ch, _, err := r.ReadRune()
	for ch == '#' && err == nil {
		skipUntil(r, func(r rune) bool { return r != '\n' })
		// Check if the next rune after the comment is either a newline or an
		// EOF. Panic for any other error since we don't know how to fix it.
		ch, _, err = r.ReadRune()
		if ch != '\n' {
			return
		}
		skipUntil(r, unicode.IsSpace)
		ch, _, err = r.ReadRune()
	}
	if err == nil {
		r.UnreadRune()
	}
}

// Advances the reader until a token can be formed and returns the token.
// Panics in case of unexpected errors.
func nextToken(r *strings.Reader, prev *token) *token {
	skipComments(r)
	var ch, _, err = r.ReadRune()
	var str string

	// Check if we reached EOF
	if err != nil {
		if err == io.EOF {
			return &token{cat: eof}
		} else {
			panic(err)
		}
	}

	// If the previous token is a '(', consume everything until a ')' or an EOF.
	// Eventually returns a token or panics in case of a non-EOF error.
	if prev != nil && prev.cat == parensOpen {
		// Skipped all whitespace and reached a closing parenthesis, there's no
		// command inside.
		if ch == ')' {
			return &token{cat: parensClose}
		}
		r.UnreadRune()
		str = strings.TrimSpace(consumeWhile(r, func(r rune) bool { return r != ')' }))
		if len(str) > 0 {
			return &token{command, str}
		}
	}
	// Starting a string
	if ch == '"' {
		// Take the string literally, don't trim space
		str = consumeWhile(r, func(r rune) bool { return r != '"' })
		ch, _, err = r.ReadRune()
		// If we reached the end and it's not a closing quote the syntax is wrong
		if ch != '"' {
			log.SetOutput(os.Stderr)
			log.Fatal("Expected closing '\"'")
		}
		return &token{strng, str}
	}
	if unicode.IsLetter(ch) {
		// If the rune is a letter, read all letters until the first non-letter
		// character, and then match the string against the available literal tokens.
		r.UnreadRune()
		str = strings.TrimSpace(consumeWhile(r, func(r rune) bool { return unicode.IsLetter(r) || r == '_' }))
		return matchToken(str)
	} else if unicode.IsNumber(ch) {
		// If it's a number, consume all numbers
		r.UnreadRune()
		str = strings.TrimSpace(consumeWhile(r, unicode.IsNumber))
		return &token{number, str}
	} else {
		// Handle the rest of cases, which are either punctuation or garbage.
		return matchToken(string(ch))
	}
}

func dumpTokens(tokens []*token) {
	for _, token := range tokens {
		switch token.cat {
		case parensOpen:
			fmt.Print("Token: (\n")
		case parensClose:
			fmt.Print("Token: )\n")
		case period:
			fmt.Print("Token: .\n")
		case assignment:
			fmt.Print("Token: =\n")
		case step:
			fmt.Printf("Token: step [%v]\n", token.value)
		case stage:
			fmt.Printf("Token: stage [%v]\n", token.value)
		case command:
			fmt.Printf("Token: command [%v]\n", token.value)
		case comment:
			fmt.Print("Token: comment [%v]\n")
		case identifier:
			fmt.Printf("Token: identifier [%v]\n", token.value)
		case strng:
			fmt.Printf("Token: string [%v]\n", token.value)
		case number:
			fmt.Printf("Token: number [%v]\n", token.value)
		case eof:
			fmt.Print("Token: EOF\n")
		case unknown:
			fmt.Print("Token: unknown\n")
		}
	}
}

// Tokenizes the script until EOF or an error occurs.
// Consumes all the input, the eof token is always the last one.
func tokenize(script string) []*token {
	var r = strings.NewReader(script)
	var tok, prev *token
	var tokens []*token

	for tok = nextToken(r, nil); tok.cat != eof; tok = nextToken(r, prev) {
		tokens = append(tokens, tok)
		prev = tok
	}
	tokens = append(tokens, tok)
	return tokens
}

type parserState int

const (
	parseStatement parserState = iota
	parseAction
	parseAssignment
	parseCommand
)

// Parses the tokens into a list of actions to execute. Uses a simple state
// machine to keep track of what tokens to expect and will try to consume as
// many tokens as possible, however it stops at the first error. Note that a as
// long as the tokens follow the grammar it's not incorrect to parse a partial
// list of tokens, it just won't return any incomplete actions.
// In case of error returns an empty slice and err is non-nil.
func parseTokens(tokens []*token) (build *BuildScript, err error) {
	var state parserState = parseStatement
	var action BuildAction
	var variableName string

	build = NewBuild()

	for i := 0; i < len(tokens); i++ {
		switch state {
		case parseStatement:
			switch tokens[i].cat {
			case eof:
				continue
			case step:
				action = BuildAction{step: strToStep(tokens[i].value)}
				state = parseAction
			case identifier:
				variableName = tokens[i].value
				state = parseAssignment
			default:
				err = fmt.Errorf("Expected step (build, package, launch)")
				return
			}
		case parseAction:
			if tokens[i].cat != period {
				err = fmt.Errorf("Expected '.'")
				return
			}
			i++
			if i >= len(tokens) || tokens[i].cat != stage {
				err = fmt.Errorf("Expected stage (before, after)")
				return
			}
			action.stage = strToStage(tokens[i].value)
			state = parseCommand
		case parseCommand:
			if tokens[i].cat != parensOpen {
				err = fmt.Errorf("Expected '('")
				return
			}
			i++
			if i >= len(tokens) || tokens[i].cat != command {
				err = fmt.Errorf("Expected a command to execute for this action")
				return
			}
			action.command = tokens[i].value
			i++
			if i >= len(tokens) || tokens[i].cat != parensClose {
				err = fmt.Errorf("Expected ')'")
				return
			}
			build.Actions = append(build.Actions, action)
			state = parseStatement
		case parseAssignment:
			if tokens[i].cat != assignment {
				err = fmt.Errorf("Expected assignment token '='")
				return
			}
			i++
			if i >= len(tokens) {
				err = fmt.Errorf("Missing assignment value")
				return
			}
			switch tokens[i].cat {
			case strng:
				build.StrVars[variableName] = tokens[i].value
			case number:
				build.IntVars[variableName], err = strconv.Atoi(tokens[i].value)
				if err != nil {
					return
				}
			default:
				err = fmt.Errorf("Expected a string or an integer value")
				return
			}
			state = parseStatement
		}
	}
	return
}
