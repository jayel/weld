# Installation

    cd $GOPATH
    git clone https://jayel@bitbucket.org/jayel/weld.git bitbucket.org/jayel/weld
    cd bitbucket.org/jayel/weld/cmd/weld
    go install

## Requirements

Weld was developed and tested with:

- Go 1.9.1
- Android Build Tools 26.0.2
- Android Platform Tools 26.0.1
- Android Emulator 26.1.4
- Android SDK Platform 15
- Android SDK Platform 26

To build and run the tests, you'll need to run:

    go get github.com/stretchr/testify/assert

If you want to modify the assets (generally not needed), you'll also need:

    go get -u github.com/jteeuwen/go-bindata/...

# Introduction

Weld is a bare bones build system for Android.

The project is split in two parts:

- A library that implements the build system (cmd/weld)
- A command line tool that uses the library and takes input from the user and  
  passes it into the library (pkg)

The typical work flow with Weld is:

1. Initialize a project anywhere in the filesystem. This creates a "weld.it" file,  
    holding the project's configuration. The directory containing this file is  
    called the root directory.
2. Build in the root directory with `weld build`
3. Package the project with `weld package`
4. Start an emulator or connect an Android device to a USB port
5. Launch the project with `weld launch`

# Weld Script

Weld works with a minimal configuration file called "weld.it". This name is fixed  
and cannot be configured. This file contains project variables such as application  
name, package, and so on, and actions.

## Variables

Variable names are case sensitive and currently there's only support for string  
and integer variables. The syntax is as follows:

<var> = [0-9]+
<var> = "string"

The built-in variables so far are:

`appName` (string)
    application name, name of the main activity

`keystore` (string)
    full path to the keystore file

`keystorePassword` (string)
    password of the keystore file

`keystoreAlias` (string)
    alias of the keystore to use

`pkg` (string)
    Java package of the project

`targetPlatform` (string)
    target Android platform to use (ie: "android-15")

`apt` (string)
    command line tool of the same name

`adb` (string)
    command line tool of the same name

`dx` (string)
    command line tool of the same name

`jarsigner` (string)
    command line tool of the same name

`javac` (string)
    command line tool of the same name

`zipalign` (string)
    command line tool of the same name

`ANDROID_SDK_ROOT` (string)
    Value of the environment variable

## Actions

Actions are commands that will be executed at some point during the build. The  
build is split into three steps: build, package, launch.  
Additionally each step has a stage: before and after.  
The format for an action is:

`
<stage>.<step> (
    <command>
)
`

For example:

`
compile.before (
    touch build.txt
)
`

This will execute the command right before building the project. There can be  
multiple commands per phase, where a phase is a (step, stage) pair.

Note that commands with strings and quotes will probably not work (for example,  
echo "this is a test").

# Building

    cd cmd/weld
    go install

To run it, make sure ${GOPATH}/bin is in $PATH.

# Running

## Initializing a project

From anywhere in the filesystem where you have write permissions run:

    weld [args] init

All arguments are optional, but if you want to launch the project you'll need to  
sign the package, so at a minimum you need to pass the arguments for the  
keystore. You can also run `weld init` without arguments and edit the `weld.it`  
file yourself.

It is also recommended to create a subfolder with `-r <name>` so as to not  
pollute your current folder.

Note that all arguments go *before* the command, init in this case. Generally  
the init command will look like:

    weld -k <keystore> -ka <alias> -kp <password> -n <name> -p <package> -r <root> init

## Building and Packaging

Go to the folder that contains the generated `weld.it` file and run:

    weld build
    weld package

## Deploying

Start an emulator or connect a device suitable for testing and run:

    weld launch

If you have multiple devices connected, you can specify them with the -e, -d, or  
-s flags.

## Help

To see more details about the available commands run

    weld help [command]

# Testing

Internal tests:

    cd pkg
    go test

See the file `test/e2e.sh` to do a full run automatically. Edit the keystore  
values to your system and make sure you have a running emulator before running  
this script.
