#!/bin/bash -e
# These are dummy values. Don't commit changes on this file if you change
# them to suit your system.
weld -k /home/blue/.android/debug.keystore -kp android -ka androidkey -p com.example.test -n AndroidApp -r tmp init
cd tmp
weld build
echo "Build: ok"
weld package
echo "Package: ok"
#cd ${ANDROID_SDK_ROOT}/emulator
#emulator -avd test2 -gpu off &
#cd -
weld launch
echo "Launch: ok"
if [ -f build-done.txt ]; then
    echo "Build actions: ok"
else
    echo "Build actions: failed"
fi
if [ -f packaging-done.txt ]; then
    echo "Package actions: ok"
else
    echo "Package actions: failed"
fi
