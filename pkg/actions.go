package weld

type actionStep int

const (
	stepBuild actionStep = iota
	stepPackage
	stepLaunch
)

func strToStep(str string) actionStep {
	switch str {
	case "build":
		return stepBuild
	case "package":
		return stepPackage
	case "launch":
		return stepLaunch
	default:
		panic("Unknown step")
	}
}

type actionStage int

const (
	stageBefore actionStage = iota
	stageAfter
)

func strToStage(str string) actionStage {
	switch str {
	case "before":
		return stageBefore
	case "after":
		return stageAfter
	default:
		panic("Unknown step")
	}
}

// Represents a command to execute at some point in the build process.
type BuildAction struct {
	step    actionStep
	stage   actionStage
	command string
}

type ByPhase []BuildAction

// Implement Sort interface to be able to sort build actions by their step and
// stage. Steps go: [build, package, launch]. Stages: [before, after]
func (a ByPhase) Len() int      { return len(a) }
func (a ByPhase) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByPhase) Less(i, j int) bool {
	if a[i].step < a[j].step {
		return true
	} else if a[i].step > a[j].step {
		return false
	} else {
		if a[i].stage < a[j].stage {
			return true
		} else {
			return false
		}
	}
}

// Returns a slice with the actions scheduled for a specific build step. If
// there are no actions to perform, returns an empty slice.
// Assumes the actions slice is sorted ByPhase.
func getActionsForPhase(step actionStep, stage actionStage, actions []BuildAction) []BuildAction {
	start, end := -1, -1

	for i, action := range actions {
		if action.step == step && action.stage == stage {
			if start == -1 {
				start = i
				end = i + 1
			} else {
				end++
			}
		}
	}
	if start >= 0 && end >= 0 {
		return actions[start:end]
	}
	return nil
}
