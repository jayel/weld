package weld

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"io"
	"strings"
	"testing"
	"unicode"
)

func TestSkipWhitespace(t *testing.T) {
	tests := []struct {
		str string
		ch  rune
		err error
	}{
		{"   skip", 's', nil},
		{"\n\t \nskip", 's', nil},
		{" \n\t", 0, io.EOF},
		{"   ", 0, io.EOF},
		{"\n\n\n", 0, io.EOF},
	}

	for _, test := range tests {
		r := strings.NewReader(test.str)
		skipUntil(r, unicode.IsSpace)
		ch, _, err := r.ReadRune()
		if test.err != nil {
			assert.NotEqual(t, nil, err)
		} else {
			assert.Equal(t, nil, err)
			assert.Equal(t, test.ch, ch)
		}
	}
}

func TestSkipComments(t *testing.T) {
	tests := []struct {
		str string
		ch  rune
		err error
	}{
		{"#   skip", 0, io.EOF},
		{"# a comment\n\t \nskip", 's', nil},
		{"# a coments\n\t  # then another\n\n build", 'b', nil},
		{"####", 0, io.EOF},
		{"\n\n\n", 0, io.EOF},
	}

	for _, test := range tests {
		r := strings.NewReader(test.str)
		skipComments(r)
		ch, _, err := r.ReadRune()
		if test.err != nil {
			assert.NotEqual(t, nil, err)
		} else {
			assert.Equal(t, nil, err)
			assert.Equal(t, test.ch, ch)
		}
	}
}

func TestNextToken(t *testing.T) {
	tests := []struct {
		str      string
		expected []token
	}{
		{"build", []token{{step, "build"}, {cat: eof}}},
		{"build after", []token{{step, "build"}, {stage, "after"}, {cat: eof}}},
		{"build.after", []token{{step, "build"}, {cat: period}, {stage, "after"}, {cat: eof}}},
		{"build launch package", []token{{step, "build"}, {step, "launch"}, {step, "package"}, {cat: eof}}},
		{"before after", []token{{stage, "before"}, {stage, "after"}, {cat: eof}}},
		{"before #comment\nafter", []token{{stage, "before"}, {stage, "after"}, {cat: eof}}},
		{"(\n\t cp ~/home/.vimrc \t/tmp \n\t )", []token{{cat: parensOpen}, {command, "cp ~/home/.vimrc \t/tmp"}, {cat: parensClose}, {cat: eof}}},
		{"( )", []token{{cat: parensOpen}, {cat: parensClose}, {cat: eof}}},
		{"build.ojhprej garbage", []token{{step, "build"}, {cat: period}, {identifier, "ojhprej"}, {identifier, "garbage"}, {cat: eof}}},
		{"variable=123", []token{{identifier, "variable"}, {cat: assignment}, {number, "123"}, {cat: eof}}},
		{"variable=\"string value\"", []token{{identifier, "variable"}, {cat: assignment}, {strng, "string value"}, {cat: eof}}},
	}
	for _, test := range tests {
		var tokens []*token
		var prev *token = nil
		var current *token = nil
		r := strings.NewReader(test.str)
		current = nextToken(r, prev)
		for ; current.cat != eof; current = nextToken(r, prev) {
			tokens = append(tokens, current)
			prev = current
		}
		tokens = append(tokens, current)
		assert.Equal(t, len(tokens), len(test.expected), "Number of tokens not different")
		for i, _ := range tokens {
			assert.Equal(t, test.expected[i].cat, tokens[i].cat, "Token type doesn't match")
			assert.Equal(t, test.expected[i].value, tokens[i].value, "Token value doesn't match")
		}
	}
}

func TestTokenize(t *testing.T) {
	tests := []struct {
		str      string
		expected []token
	}{
		{"build", []token{{step, "build"}, {cat: eof}}},
		{"build after", []token{{step, "build"}, {stage, "after"}, {cat: eof}}},
		{"build.after", []token{{step, "build"}, {cat: period}, {stage, "after"}, {cat: eof}}},
		{"build launch package", []token{{step, "build"}, {step, "launch"}, {step, "package"}, {cat: eof}}},
		{"before after", []token{{stage, "before"}, {stage, "after"}, {cat: eof}}},
		{"before #comment\nafter", []token{{stage, "before"}, {stage, "after"}, {cat: eof}}},
		{"(\n\t cp ~/home/.vimrc \t/tmp \n\t )", []token{{cat: parensOpen}, {command, "cp ~/home/.vimrc \t/tmp"}, {cat: parensClose}, {cat: eof}}},
		{"( )", []token{{cat: parensOpen}, {cat: parensClose}, {cat: eof}}},
		{"build.ojhprej garbage", []token{{step, "build"}, {cat: period}, {identifier, "ojhprej"}, {identifier, "garbage"}, {cat: eof}}},
		{"variable=123", []token{{identifier, "variable"}, {cat: assignment}, {number, "123"}, {cat: eof}}},
		{"variable=\"string value\"", []token{{identifier, "variable"}, {cat: assignment}, {strng, "string value"}, {cat: eof}}},
	}
	for _, test := range tests {
		tokens := tokenize(test.str)
		assert.Equal(t, len(tokens), len(test.expected), "Number of tokens different")
		for i, _ := range tokens {
			assert.Equal(t, tokens[i].cat, test.expected[i].cat, "Token type doesn't match")
			assert.Equal(t, tokens[i].value, test.expected[i].value, "Token value doesn't match")
		}
	}
}

func TestParseTokens(t *testing.T) {
	tests := []struct {
		input       []*token
		err         error
		actionCount int
		strVarCount int
		intVarCount int
	}{
		{[]*token{{step, "build"}, {cat: period}, {stage, "before"}}, nil, 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {stage, "before"}, {cat: parensOpen},
			{command, "cp ~/.vimrc /tmp"}, {cat: parensClose}}, nil, 1, 0, 0},
		{[]*token{{step, "package"}, {cat: period}, {stage, "before"}, {cat: parensOpen},
			{command, "cp ~/.vimrc /tmp"}, {cat: parensClose}, {step, "package"}, {cat: period},
			{stage, "before"}, {cat: parensOpen}, {command, "cp /media/external/vimrc ~/.vimrc"},
			{cat: parensClose}}, nil, 2, 0, 0},
		{[]*token{{identifier, "keystore"}, {cat: assignment}, {strng, "keystore.dat"}}, nil, 0, 1, 0},
		{[]*token{{identifier, "keystore"}, {cat: assignment}, {strng, "keystore.dat"},
			{identifier, "targetId"}, {cat: assignment}, {number, "12"}}, nil, 0, 1, 1},
		{[]*token{{identifier, "keystore"}, {cat: assignment}, {strng, "keystore.dat"},
			{identifier, "targetId"}, {cat: assignment}, {number, "12"}}, nil, 0, 1, 1},
		// Error cases
		// TODO: combinatorial input generator
		{[]*token{{cat: period}}, errors.New(""), 0, 0, 0},
		{[]*token{{stage, "before"}}, errors.New(""), 0, 0, 0},
		{[]*token{{cat: command}}, errors.New(""), 0, 0, 0},
		{[]*token{{cat: parensOpen}}, errors.New(""), 0, 0, 0},
		{[]*token{{cat: parensClose}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {stage, "before"}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: parensOpen}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: parensClose}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: command}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {cat: command}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {cat: parensOpen}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {cat: parensClose}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {cat: period}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {stage, "before"}, {cat: command}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {stage, "before"}, {cat: parensOpen}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {stage, "before"}, {cat: parensClose}}, errors.New(""), 0, 0, 0},
		{[]*token{{step, "build"}, {cat: period}, {stage, "before"}, {cat: parensOpen}, {command, "cp ~/.vimrc /tmp"}, {cat: parensClose}, {cat: parensOpen}}, errors.New(""), 1, 0, 0},
	}
	assert := assert.New(t)

	for _, test := range tests {
		build, err := parseTokens(test.input)
		if test.err != nil {
			assert.NotEqual(err, nil, "Parser error different than expected")
		} else {
			assert.Equal(err, nil, "Parser error")
		}
		assert.Equal(len(build.Actions), test.actionCount, "Unexpected number of actions parsed")
		assert.Equal(len(build.IntVars), test.intVarCount, "Unexpected number of int variables parsed")
		assert.Equal(len(build.StrVars), test.strVarCount, "Unexpected number of string variables parsed")
	}
}
