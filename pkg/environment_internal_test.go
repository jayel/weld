package weld

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDetect(t *testing.T) {
	tools := []toolParams{
		// Any *nix system should have ls installed
		{false, toolAapt, "ls", []string{"-l"}},
	}
	result := Detect(tools, false)
	assert.Equal(t, 1, len(result))
}
