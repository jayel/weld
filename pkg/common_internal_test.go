package weld

import (
    "testing"
    "errors"
    "github.com/stretchr/testify/assert"
)

func TestRunOrFail(t *testing.T) {
    tests := []struct {
        command string
        err error
    }{
        {"ls", nil},
        {"rknugrebjbdjgbkjweb", errors.New("")},
    }
    for _, test := range tests {
        err := runOrFail(false, false, test.command)
        if test.err != nil {
            assert.NotEqual(t, nil, err)
        } else {
            assert.Equal(t, nil, err)
        }
    }
}
